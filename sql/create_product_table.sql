IF OBJECT_ID('dbo.Product', 'U') IS NOT NULL
  DROP TABLE Product
GO

CREATE TABLE Product
(
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name varchar(50) NOT NULL,
	ProductDescription text,
	Active bit NOT NULL -- ms sql lacks a proper bool type
)
GO
