IF OBJECT_ID('dbo.Feature', 'U') IS NOT NULL
  DROP TABLE Feature
GO

CREATE TABLE Feature
(
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name varchar(50) NOT NULL,
	FeatureDescription text,
	Active bit NOT NULL -- ms sql lacks a proper bool type
)
GO
