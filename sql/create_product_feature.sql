IF OBJECT_ID('dbo.ProductFeature', 'U') IS NOT NULL
  DROP TABLE ProductFeature
GO

CREATE TABLE ProductFeature
(
	FeatureId int NOT NULL REFERENCES dbo.Feature(Id),
	ProductId int NOT NULL REFERENCES dbo.Product(Id),
	CONSTRAINT ProductFeaturePkey PRIMARY KEY (FeatureId, ProductId)
)
GO
