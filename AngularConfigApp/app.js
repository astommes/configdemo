angular.module('project', ['restangular', 'ngRoute']).
  config(function($routeProvider, RestangularProvider) {
    $routeProvider.
      when('/', {
        controller:ListCtrl,
        templateUrl:'list.html'
      }).
      when('/edit/:productId', {
        controller:EditCtrl,
        templateUrl:'detail.html',
        resolve: {
          product: function(Restangular, $route){
              return Restangular.one('product/').get({Id:$route.current.params.productId});
          }
        }
      }).
      when('/new', {controller:CreateCtrl, templateUrl:'detail.html'}).
      otherwise({redirectTo:'/'});

      RestangularProvider.setBaseUrl('http://wcfconfigservice.azurewebsites.net/ProductService.svc/');
  });


function ListCtrl($scope, Restangular) {
   $scope.products = Restangular.all("product").getList().$object;
}


function CreateCtrl($scope, $location, Restangular) {
  $scope.save = function() {
      Restangular.all('product/post').post({ product: $scope.product }).then(function(product) {
      $location.path('/list');
    });
  }
}

function EditCtrl($scope, $location, Restangular, product) {
  var original = product;
  $scope.product = Restangular.copy(original);


  $scope.isClean = function() {
    return angular.equals(original, $scope.product);
  }

  $scope.destroy = function() {
    original.remove().then(function() {
      $location.path('/list');
    });
  };

  $scope.save = function() {
      Restangular.all('product/put').customPUT({ product: $scope.product }).then(function (product) {
          $location.path('/list');
      });
  };
}
