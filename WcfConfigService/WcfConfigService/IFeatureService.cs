﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WcfConfigService.DomainObjects;

namespace WcfConfigService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IFeatureService
    {
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/feature")]
        List<Feature> GetFeatures();

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/freature/?id={id}")]
        Feature GetFeature(int id);

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/feature/post",
            Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void PostFeature(Feature feature);

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/feature/put",
            Method = "PUT",
            BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        Feature PutFeature(Feature feature);
    }
}
