﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WcfConfigService.DomainObjects;

namespace WcfConfigService
{
    public class Service
    {
        public string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConfigDb"].ConnectionString;
            }
        }
    }
}
