﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;

namespace WcfConfigService.DomainObjects
{
    [DataContract]
    public class Feature
    {
        [DataMember]
        public int Id { get; set; }
       
        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public string FeatureDescription { get; set; }
        
        [DataMember]
        public bool Active { get; set; }
    }
}