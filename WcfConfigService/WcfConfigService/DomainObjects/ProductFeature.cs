﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;

namespace WcfConfigService.DomainObjects
{
    [DataContract]
    public class ProductFeature
    {
        [DataMember]
        public int ProductId { get; set; }

        [DataMember]
        public int FeatureId { get; set; }
    }
}