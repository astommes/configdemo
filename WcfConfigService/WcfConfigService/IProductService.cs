﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WcfConfigService.DomainObjects;

namespace WcfConfigService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IProductService" in both code and config file together.
    [ServiceContract]
    public interface IProductService
    {
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, 
            UriTemplate = "/product")]
        List<Product> GetProducts();

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, 
            UriTemplate = "/product/?id={id}")]
        Product GetProduct(int id);

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json, 
            UriTemplate = "/product/post", 
            Method="POST", 
            BodyStyle=WebMessageBodyStyle.WrappedRequest)]
        void PostProduct(Product product);

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/product/put",
            Method = "PUT",
            BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        Product PutProduct(Product product);
    }
}
