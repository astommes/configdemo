﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Dapper;
using System.Configuration;
using WcfConfigService.DomainObjects;
using System.Web;

namespace WcfConfigService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ProductService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ProductService.svc or ProductService.svc.cs at the Solution Explorer and start debugging.
    public class ProductService : Service, IProductService
    {
        public List<Product> GetProducts()
        {
            IEnumerable<Product> products;
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                products = sqlConnection.Query<Product>("SELECT * FROM Product");
                sqlConnection.Close();
            }
            if (products == null || !products.Any())
            {
                throw new HttpException(404, "No products found");
            }
            return products.ToList();
        }

        public Product GetProduct(int id)
        {
            Product product;
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                product = sqlConnection.Query<Product>(@"SELECT * FROM Product WHERE Id = @id",
                    new { id }).SingleOrDefault();
                sqlConnection.Close();
                if (product == null)
                {
                    throw new HttpException(404, "Product not found");
                }
                return product;
            }
        }

        public void PostProduct(Product product)
        {
            var processQuery = @"INSERT INTO Product(Name, ProductDescription, Active)
                                 VALUES (@Name, @ProductDescription, @Active)";
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                sqlConnection.Query<int>(processQuery, product);
                sqlConnection.Close();
            }
        }

        public Product PutProduct(Product product)
        {
            var processQuery = 
                    @"UPDATE Product 
                    SET Name = @Name, ProductDescription = @ProductDescription, Active = @Active
                    WHERE Id = @Id";
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                sqlConnection.Query<int>(processQuery, product);
                sqlConnection.Close();
            }
            return GetProduct(product.Id);
        }
    }
}
