﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfConfigService.DomainObjects;
using Dapper;
using System.Web;

namespace WcfConfigService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class FeatureService : Service, IFeatureService
    {
        public List<Feature> GetFeatures()
        {
            IEnumerable<Feature> Features;
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                Features = sqlConnection.Query<Feature>("SELECT * FROM Feature");
                sqlConnection.Close();
            }
            if (Features == null || !Features.Any())
            {
                throw new HttpException(404, "No Features found");
            }
            return Features.ToList();
        }

        public Feature GetFeature(int id)
        {
            Feature Feature;
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                Feature = sqlConnection.Query<Feature>(@"SELECT * FROM Feature WHERE Id = @id",
                    new { id }).SingleOrDefault();
                sqlConnection.Close();
                if (Feature == null)
                {
                    throw new HttpException(404, "Feature not found");
                }
                return Feature;
            }
        }

        public void PostFeature(Feature Feature)
        {
            var processQuery = @"INSERT INTO Feature(Name, FeatureDescription, Active)
                                 VALUES (@Name, @FeatureDescription, @Active)";
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                sqlConnection.Query<int>(processQuery, Feature);
                sqlConnection.Close();
            }
        }

        public Feature PutFeature(Feature Feature)
        {
            var processQuery =
                    @"UPDATE Feature 
                    SET Name = @Name, FeatureDescription = @FeatureDescription, Active = @Active
                    WHERE Id = @Id";
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                sqlConnection.Query<int>(processQuery, Feature);
                sqlConnection.Close();
            }
            return GetFeature(Feature.Id);
        }
    }
}
